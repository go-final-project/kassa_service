package client

import (
	"gitlab.com/GoFinalProject/kassa_service/config"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/kassa_service/genproto/product_service"
	"gitlab.com/GoFinalProject/kassa_service/genproto/stock_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// Organization service
	BranchService() organization_service.BranchServiceClient
	SalesAddressService() organization_service.SalesAddressServiceClient
	ProviderService() organization_service.ProviderServiceClient
	EmployeeService() organization_service.EmployeeServiceClient

	// Product service
	BrandService() product_service.BrandServiceClient
	CategoryService() product_service.CategoryServiceClient
	ProductService() product_service.ProductServiceClient

	// Stock service
	ComingProductService() stock_service.ComingProductServiceClient
	ComingService() stock_service.ComingServiceClient
	RemainingService() stock_service.RemainingServiceClient

	// Kassa service
	SaleService() kassa_service.SaleServiceClient
	SaleProductService() kassa_service.SaleProductServiceClient
	SmenaService() kassa_service.SmenaServiceClient
	TransactionService() kassa_service.TransactionServiceClient
	PaymentService() kassa_service.PaymentServiceClient
}

type grpcClients struct {
	// Organization service
	branchService       organization_service.BranchServiceClient
	salesAddressService organization_service.SalesAddressServiceClient
	providerService     organization_service.ProviderServiceClient
	employeeService     organization_service.EmployeeServiceClient

	// Product service
	brandService    product_service.BrandServiceClient
	categoryService product_service.CategoryServiceClient
	productService  product_service.ProductServiceClient

	// Stock service
	comingProductService stock_service.ComingProductServiceClient
	comingService        stock_service.ComingServiceClient
	remainingService     stock_service.RemainingServiceClient

	// Kassa service
	saleService        kassa_service.SaleServiceClient
	saleProductService kassa_service.SaleProductServiceClient
	smenaService       kassa_service.SmenaServiceClient
	transactionService kassa_service.TransactionServiceClient
	paymentService     kassa_service.PaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Organization service
	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceHost+cfg.OrganizationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	// Product service
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	// Stock service
	connStockService, err := grpc.Dial(
		cfg.StockServiceHost+cfg.StockGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	// Kassa service
	connKassaService, err := grpc.Dial(
		cfg.KassaServiceHost+cfg.KassaGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Organization service
		branchService:       organization_service.NewBranchServiceClient(connOrganizationService),
		salesAddressService: organization_service.NewSalesAddressServiceClient(connOrganizationService),
		providerService:     organization_service.NewProviderServiceClient(connOrganizationService),
		employeeService:     organization_service.NewEmployeeServiceClient(connOrganizationService),

		// Product service
		brandService:    product_service.NewBrandServiceClient(connProductService),
		categoryService: product_service.NewCategoryServiceClient(connProductService),
		productService:  product_service.NewProductServiceClient(connProductService),

		// Stock service
		comingProductService: stock_service.NewComingProductServiceClient(connStockService),
		comingService:        stock_service.NewComingServiceClient(connStockService),
		remainingService:     stock_service.NewRemainingServiceClient(connStockService),

		// Kassa service
		saleService:        kassa_service.NewSaleServiceClient(connKassaService),
		saleProductService: kassa_service.NewSaleProductServiceClient(connKassaService),
		smenaService:       kassa_service.NewSmenaServiceClient(connKassaService),
		transactionService: kassa_service.NewTransactionServiceClient(connKassaService),
		paymentService:     kassa_service.NewPaymentServiceClient(connKassaService),
	}, nil
}
func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) SalesAddressService() organization_service.SalesAddressServiceClient {
	return g.salesAddressService
}

func (g *grpcClients) ProviderService() organization_service.ProviderServiceClient {
	return g.providerService
}

func (g *grpcClients) EmployeeService() organization_service.EmployeeServiceClient {
	return g.employeeService
}

func (g *grpcClients) BrandService() product_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) ComingProductService() stock_service.ComingProductServiceClient {
	return g.comingProductService
}

func (g *grpcClients) ComingService() stock_service.ComingServiceClient {
	return g.comingService
}

func (g *grpcClients) RemainingService() stock_service.RemainingServiceClient {
	return g.remainingService
}

func (g *grpcClients) SaleService() kassa_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() kassa_service.SaleProductServiceClient {
	return g.saleProductService
}

func (g *grpcClients) SmenaService() kassa_service.SmenaServiceClient {
	return g.smenaService
}

func (g *grpcClients) TransactionService() kassa_service.TransactionServiceClient {
	return g.transactionService
}

func (g *grpcClients) PaymentService() kassa_service.PaymentServiceClient {
	return g.paymentService
}
