package grpc

import (
	"gitlab.com/GoFinalProject/kassa_service/config"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/grpc/client"
	"gitlab.com/GoFinalProject/kassa_service/grpc/service"
	"gitlab.com/GoFinalProject/kassa_service/pkg/logger"
	"gitlab.com/GoFinalProject/kassa_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	kassa_service.RegisterSaleServiceServer(grpcServer, service.NewSaleService(cfg, log, strg, srvc))
	kassa_service.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(cfg, log, strg, srvc))
	kassa_service.RegisterPaymentServiceServer(grpcServer, service.NewPaymentService(cfg, log, strg, srvc))
	kassa_service.RegisterSmenaServiceServer(grpcServer, service.NewSmenaService(cfg, log, strg, srvc))
	kassa_service.RegisterTransactionServiceServer(grpcServer, service.NewTransactionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
