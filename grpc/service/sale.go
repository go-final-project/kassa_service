package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/kassa_service/config"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/kassa_service/grpc/client"
	"gitlab.com/GoFinalProject/kassa_service/pkg/logger"
	"gitlab.com/GoFinalProject/kassa_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SaleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedSaleServiceServer
}

func NewSaleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SaleService {
	return &SaleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SaleService) Create(ctx context.Context, req *kassa_service.SaleCreate) (*kassa_service.Sale, error) {
	u.log.Info("====== Sale Create ======", logger.Any("req", req))
	branch, err := u.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{
		Id: req.BranchId,
	})
	if err != nil {
		u.log.Error("Error While Create Sale: u.services.BranchService().GetById", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	req.SaleId = branch.BranchCode
	smena, err := u.services.SmenaService().GetById(ctx, &kassa_service.SmenaPrimaryKey{Id: req.SmenaId})
	if smena.Status == "closed" {
		return nil, status.Error(codes.Canceled, "У вас нет открытая смена")
	}
	resp, err := u.strg.Sale().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Sale: u.strg.Sale().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (u *SaleService) GetById(ctx context.Context, req *kassa_service.SalePrimaryKey) (*kassa_service.Sale, error) {
	u.log.Info("====== Sale Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Sale().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Get By ID: u.strg.Sale().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (u *SaleService) GetList(ctx context.Context, req *kassa_service.SaleGetListRequest) (*kassa_service.SaleGetListResponse, error) {
	u.log.Info("====== Sale Get List ======", logger.Any("req", req))

	resp, err := u.strg.Sale().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Get List: u.strg.Sale().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleService) Update(ctx context.Context, req *kassa_service.SaleUpdate) (*kassa_service.Sale, error) {
	u.log.Info("====== Sale Update ======", logger.Any("req", req))

	resp, err := u.strg.Sale().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Update: u.strg.Sale().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleService) Delete(ctx context.Context, req *kassa_service.SalePrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Sale Delete ======", logger.Any("req", req))

	err := u.strg.Sale().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Sale Delete: u.strg.Sale().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
