package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/kassa_service/config"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/grpc/client"
	"gitlab.com/GoFinalProject/kassa_service/pkg/logger"
	"gitlab.com/GoFinalProject/kassa_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SmenaService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedSmenaServiceServer
}

func NewSmenaService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SmenaService {
	return &SmenaService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SmenaService) Create(ctx context.Context, req *kassa_service.SmenaCreate) (*kassa_service.Smena, error) {
	u.log.Info("====== Smena Create ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Smena: u.strg.Smena().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) GetById(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (*kassa_service.Smena, error) {
	u.log.Info("====== Smena Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Smena().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Smena Get By ID: u.strg.Smena().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) GetList(ctx context.Context, req *kassa_service.SmenaGetListRequest) (*kassa_service.SmenaGetListResponse, error) {
	u.log.Info("====== Smena Get List ======", logger.Any("req", req))

	resp, err := u.strg.Smena().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Smena Get List: u.strg.Smena().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) Update(ctx context.Context, req *kassa_service.SmenaUpdate) (*kassa_service.Smena, error) {
	u.log.Info("====== Smena Update ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Smena Update: u.strg.Smena().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) Delete(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Smena Delete ======", logger.Any("req", req))

	err := u.strg.Smena().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Smena Delete: u.strg.Smena().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
