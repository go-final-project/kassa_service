CREATE TABLE sale_products (
    id UUID PRIMARY KEY,
    brand_id UUID NOT NULL,
    category_id UUID NOT NULL,
    product_name VARCHAR NOT NULL,
    barcode VARCHAR NOT NULL,
    remaining_quantity NUMERIC NOT NULL,
    quantity NUMERIC NOT NULL,
    price NUMERIC NOT NULL,
    total_price NUMERIC NOT NULL
);