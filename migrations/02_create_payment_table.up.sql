CREATE TABLE payment (
    id UUID PRIMARY KEY,
    cash NUMERIC,
    uzcard NUMERIC,
    payme NUMERIC,
    click NUMERIC,
    humo NUMERIC,
    apelsin NUMERIC,
    visa NUMERIC,
    currency VARCHAR NOT NULL,
    exchange_rate NUMERIC NOT NULL,
    sale_id UUID NOT NULL,
    total_price NUMERIC NOT NULL
);