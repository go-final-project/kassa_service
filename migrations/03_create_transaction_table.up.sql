CREATE TABLE transaction (
    id UUID PRIMARY KEY,
    cash NUMERIC,
    uzcard NUMERIC,
    payme NUMERIC,
    click NUMERIC,
    humo NUMERIC,
    apelsin NUMERIC,
    visa NUMERIC,
    smena_id UUID NOT NULL,
    total_price NUMERIC
);