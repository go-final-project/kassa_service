CREATE TABLE smena (
    id UUID PRIMARY KEY,
    smena_id VARCHAR(1),
    branch_id UUID NOT NULL,
    employee_id UUID NOT NULL,
    sales_address_id UUID NOT NULL,
    status VARCHAR DEFAULT 'opened'
);