CREATE TABLE sale (
    id UUID PRIMARY KEY,
    sale_id VARCHAR(3) NOT NULL,
    smena_id UUID REFERENCES smena("id"),
    branch_id UUID NOT NULL,
    employee_id UUID NOT NULL,
    sales_address_id UUID NOT NULL,
    status VARCHAR DEFAULT 'in_process'
);