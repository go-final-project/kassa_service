package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PaymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) *PaymentRepo {
	return &PaymentRepo{
		db: db,
	}
}

func (r *PaymentRepo) Create(ctx context.Context, req *kassa_service.PaymentCreate) (*kassa_service.Payment, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO payment(id, cash, uzcard, payme, click, humo, apelsin, visa, currency, exchange_rate, sale_id, total_price)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		req.Visa,
		req.Currency,
		req.ExchangeRate,
		req.SalesId,
		req.TotalPrice,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Payment{
		Id:           id,
		Cash:         req.Cash,
		Uzcard:       req.Uzcard,
		Payme:        req.Payme,
		Click:        req.Click,
		Humo:         req.Humo,
		Apelsin:      req.Apelsin,
		Visa:         req.Visa,
		Currency:     req.Currency,
		ExchangeRate: req.ExchangeRate,
		SalesId:      req.SalesId,
		TotalPrice:   req.TotalPrice,
	}, nil
}

func (r *PaymentRepo) GetByID(ctx context.Context, req *kassa_service.PaymentPrimaryKey) (*kassa_service.Payment, error) {
	var (
		where        string
		query        string
		id           sql.NullString
		cash         int64
		uzcard       int64
		payme        int64
		click        int64
		humo         int64
		apelsin      int64
		visa         int64
		currency     string
		exchangeRate int64
		totalPrice   int64
		saleId       string
	)
	if len(req.Id) > 0 {
		where = " WHERE id = $1"
	} else if len(req.SaleId) > 0 {
		where = " WHERE sales_id = $1"
		req.Id = req.SaleId
	}

	query = `
		SELECT
			id, cash, uzcard, payme, click, humo, apelsin, visa, currency, exchange_rate, total_price, sale_id
		FROM payment
	`
	query += where
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&visa,
		&currency,
		&exchangeRate,
		&totalPrice,
		&saleId,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Payment{
		Id:           id.String,
		Cash:         cash,
		Uzcard:       uzcard,
		Payme:        payme,
		Click:        click,
		Humo:         humo,
		Apelsin:      apelsin,
		Visa:         visa,
		Currency:     currency,
		ExchangeRate: exchangeRate,
		TotalPrice:   totalPrice,
		SalesId:      saleId,
	}, nil
}

func (r *PaymentRepo) GetList(ctx context.Context, req *kassa_service.PaymentGetListRequest) (*kassa_service.PaymentGetListResponse, error) {

	var (
		resp   = &kassa_service.PaymentGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, cash, uzcard, payme, click, humo, apelsin, visa, currency, exchange_rate, total_price, sale_id
		FROM payment
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchBySaleId != "" {
		where += ` AND sale_id ILIKE '%' || '` + req.SearchBySaleId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			cash         int64
			uzcard       int64
			payme        int64
			click        int64
			humo         int64
			apelsin      int64
			visa         int64
			currency     string
			exchangeRate int64
			totalPrice   int64
			salesId      string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&visa,
			&currency,
			&exchangeRate,
			&totalPrice,
			&salesId,
		)

		if err != nil {
			return nil, err
		}

		resp.Payments = append(resp.Payments, &kassa_service.Payment{
			Id:           id.String,
			Cash:         cash,
			Uzcard:       uzcard,
			Payme:        payme,
			Click:        click,
			Humo:         humo,
			Apelsin:      apelsin,
			Visa:         visa,
			Currency:     currency,
			ExchangeRate: exchangeRate,
			TotalPrice:   totalPrice,
			SalesId:      salesId,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *PaymentRepo) Update(ctx context.Context, req *kassa_service.PaymentUpdate) (*kassa_service.Payment, error) {

	var (
		query  string
		params map[string]interface{}
	)
	query = `
		UPDATE
			payment
		SET
		    cash = :cash,
		    uzcard = :uzcard,
		    payme = :payme,
		    click = :click,
		    humo = :humo,
		    apelsin = :apelsin,
		    visa = :visa,
		    currency = :currency,
		    exchange_rate = :exchange_rate,
		    total_price = :total_price,
		    sale_id = :sale_id
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":            req.Id,
		"cash":          req.Cash,
		"uzcard":        req.Uzcard,
		"payme":         req.Payme,
		"click":         req.Click,
		"humo":          req.Humo,
		"apelsin":       req.Apelsin,
		"visa":          req.Visa,
		"currency":      req.Currency,
		"exchange_rate": req.ExchangeRate,
		"total_price":   req.TotalPrice,
		"sale_id":       req.SalesId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &kassa_service.Payment{
		Id:           req.Id,
		Cash:         req.Cash,
		Uzcard:       req.Uzcard,
		Payme:        req.Payme,
		Click:        req.Click,
		Humo:         req.Humo,
		Apelsin:      req.Apelsin,
		Visa:         req.Visa,
		Currency:     req.Currency,
		ExchangeRate: req.ExchangeRate,
		TotalPrice:   req.TotalPrice,
		SalesId:      req.SalesId,
	}, nil
}

func (r *PaymentRepo) Delete(ctx context.Context, req *kassa_service.PaymentPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM payment WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
