package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/kassa_service/config"
	"gitlab.com/GoFinalProject/kassa_service/storage"
)

type Store struct {
	db          *pgxpool.Pool
	payment     *PaymentRepo
	sale        *SaleRepo
	saleProduct *SaleProductRepo
	smena       *SmenaRepo
	transaction *TransactionRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Sale() storage.SaleRepoI {
	if s.sale == nil {
		s.sale = NewSaleRepo(s.db)
	}
	return s.sale
}

func (s *Store) Smena() storage.SmenaRepoI {
	if s.smena == nil {
		s.smena = NewSmenaRepo(s.db)
	}
	return s.smena
}

func (s *Store) SaleProduct() storage.SaleProductRepoI {
	if s.saleProduct == nil {
		s.saleProduct = NewSaleProductRepo(s.db)
	}
	return s.saleProduct
}

func (s *Store) Payment() storage.PaymentRepoI {
	if s.payment == nil {
		s.payment = NewPaymentRepo(s.db)
	}
	return s.payment
}

func (s *Store) Transaction() storage.TransactionRepoI {
	if s.transaction == nil {
		s.transaction = NewTransactionRepo(s.db)
	}
	return s.transaction
}
