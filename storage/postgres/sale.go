package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/pkg/helper"
)

type SaleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) *SaleRepo {
	return &SaleRepo{
		db: db,
	}
}

func (r *SaleRepo) Create(ctx context.Context, req *kassa_service.SaleCreate) (*kassa_service.Sale, error) {
	count, err := r.GetList(ctx, &kassa_service.SaleGetListRequest{})
	var (
		id     = uuid.New().String()
		saleId = helper.MakeId(req.SaleId, int(count.Count+1))
		query  string
	)

	query = `
		INSERT INTO sale(id, sale_id, smena_id, branch_id, employee_id, sales_address_id, status)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`

	_, err = r.db.Exec(ctx, query,
		id,
		saleId,
		req.SmenaId,
		req.BranchId,
		req.EmployeeId,
		req.SalesAddressId,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Sale{
		Id:             id,
		SaleId:         saleId,
		SmenaId:        req.SmenaId,
		BranchId:       req.BranchId,
		EmployeeId:     req.EmployeeId,
		SalesAddressId: req.SalesAddressId,
		Status:         req.Status,
	}, nil
}

func (r *SaleRepo) GetByID(ctx context.Context, req *kassa_service.SalePrimaryKey) (*kassa_service.Sale, error) {
	var (
		query string

		saleId         string
		smenaId        sql.NullString
		branchId       sql.NullString
		employeeId     sql.NullString
		salesAddressId sql.NullString
		status         string
	)

	query = `
		SELECT
			sale_id, smena_id, branch_id, employee_id, sales_address_id, status
		FROM sale
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&saleId,
		&smenaId,
		&branchId,
		&employeeId,
		&salesAddressId,
		&status,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Sale{
		Id:             req.Id,
		SaleId:         saleId,
		SmenaId:        smenaId.String,
		BranchId:       branchId.String,
		EmployeeId:     employeeId.String,
		SalesAddressId: salesAddressId.String,
		Status:         status,
	}, nil
}

func (r *SaleRepo) GetList(ctx context.Context, req *kassa_service.SaleGetListRequest) (*kassa_service.SaleGetListResponse, error) {

	var (
		resp   = &kassa_service.SaleGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, sale_id, smena_id, branch_id, employee_id, sales_address_id, status	
		FROM sale
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByEmployee != "" {
		where += ` AND employee_id ILIKE '%' || '` + req.SearchByEmployee + `' || '%'`
	}

	if req.SearchBySmenaId != "" {
		where += ` AND smena_id ILIKE '%' || '` + req.SearchBySmenaId + `' || '%'`
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			saleId         string
			smenaId        sql.NullString
			branchId       sql.NullString
			employeeId     sql.NullString
			salesAddressId sql.NullString
			status         string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&saleId,
			&smenaId,
			&branchId,
			&employeeId,
			&salesAddressId,
			&status,
		)

		if err != nil {
			return nil, err
		}

		resp.Sales = append(resp.Sales, &kassa_service.Sale{
			Id:             id.String,
			SaleId:         saleId,
			SmenaId:        smenaId.String,
			BranchId:       branchId.String,
			EmployeeId:     employeeId.String,
			SalesAddressId: salesAddressId.String,
			Status:         status,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SaleRepo) Update(ctx context.Context, req *kassa_service.SaleUpdate) (*kassa_service.Sale, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			sale
		SET
			sale_id = :sale_id,
			smena_id = :smena_id,
		    branch_id = :branch_id,
		    employee_id = :employee_id,
		    sales_address_id = :sales_address_id,
		    status = :status
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":               req.Id,
		"sale_id":          req.SaleId,
		"smena_id":         req.SmenaId,
		"branch_id":        req.BranchId,
		"employee_id":      req.EmployeeId,
		"sales_address_id": req.SalesAddressId,
		"status":           req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &kassa_service.Sale{
		Id:             req.Id,
		SaleId:         req.SaleId,
		SmenaId:        req.SmenaId,
		BranchId:       req.BranchId,
		EmployeeId:     req.EmployeeId,
		SalesAddressId: req.SalesAddressId,
		Status:         req.Status,
	}, nil
}

func (r *SaleRepo) Delete(ctx context.Context, req *kassa_service.SalePrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sale WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
