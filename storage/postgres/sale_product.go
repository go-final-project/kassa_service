package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/pkg/helper"
)

type SaleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) *SaleProductRepo {
	return &SaleProductRepo{
		db: db,
	}
}

func (r *SaleProductRepo) Create(ctx context.Context, req *kassa_service.SaleProductCreate) (*kassa_service.SaleProduct, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO sale_products(id, brand_id, category_id, product_name, barcode, remaining_quantity, quantity, price, total_price)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.BrandId,
		req.CategoryId,
		req.ProductName,
		req.Barcode,
		req.RemainingQuantity,
		req.Quantity,
		req.Price,
		req.TotalPrice,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.SaleProduct{
		Id:                id,
		BrandId:           req.BrandId,
		CategoryId:        req.CategoryId,
		ProductName:       req.ProductName,
		Barcode:           req.Barcode,
		RemainingQuantity: req.RemainingQuantity,
		Quantity:          req.Quantity,
		Price:             req.Price,
		TotalPrice:        req.TotalPrice,
	}, nil
}

func (r *SaleProductRepo) GetByID(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) (*kassa_service.SaleProduct, error) {
	var (
		query string

		id                sql.NullString
		brandId           sql.NullString
		categoryId        sql.NullString
		productName       string
		barcode           string
		remainingQuantity int64
		quantity          int64
		price             int64
		totalPrice        int64
	)

	query = `
		SELECT
			brand_id, category_id, product_name, barcode, remaining_quantity, quantity, price, total_price
		FROM sale_products
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&brandId,
		&categoryId,
		&productName,
		&barcode,
		&remainingQuantity,
		&quantity,
		&price,
		&totalPrice,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.SaleProduct{
		Id:                id.String,
		BrandId:           brandId.String,
		CategoryId:        categoryId.String,
		ProductName:       productName,
		Barcode:           barcode,
		RemainingQuantity: remainingQuantity,
		Quantity:          quantity,
		Price:             price,
		TotalPrice:        totalPrice,
	}, nil
}

func (r *SaleProductRepo) GetList(ctx context.Context, req *kassa_service.SaleProductGetListRequest) (*kassa_service.SaleProductGetListResponse, error) {

	var (
		resp   = &kassa_service.SaleProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, brand_id, category_id, product_name, barcode, remaining_quantity, quantity, price, total_price

		FROM sale_products
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByBarcode != "" {
		where += ` AND barcode ILIKE '%' || '` + req.SearchByBarcode + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category_id ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id                sql.NullString
			brandId           sql.NullString
			categoryId        sql.NullString
			productName       string
			barcode           string
			remainingQuantity int64
			quantity          int64
			price             int64
			totalPrice        int64
		)

		err := rows.Scan(
			&brandId,
			&categoryId,
			&productName,
			&barcode,
			&remainingQuantity,
			&quantity,
			&price,
			&totalPrice,
		)

		if err != nil {
			return nil, err
		}

		resp.SaleProducts = append(resp.SaleProducts, &kassa_service.SaleProduct{
			Id:                id.String,
			BrandId:           brandId.String,
			CategoryId:        categoryId.String,
			ProductName:       productName,
			Barcode:           barcode,
			RemainingQuantity: remainingQuantity,
			Quantity:          quantity,
			Price:             price,
			TotalPrice:        totalPrice,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SaleProductRepo) Update(ctx context.Context, req *kassa_service.SaleProductUpdate) (*kassa_service.SaleProduct, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			sale_products
		SET
			brand_id = :brand_id,
			category_id = :category_id,
			product_name = :product_name,
			barcode = :barcode,
			remaining_quantity = :remaining_quantity,
			quantity = :quantity,
			price = :price ,
			total_price	= :total_price 	
		WHERE id = :id
	`

	params = map[string]interface{}{
		"brand_id":           req.BrandId,
		"category_id":        req.CategoryId,
		"product_name":       req.ProductName,
		"barcode":            req.Barcode,
		"remaining_quantity": req.RemainingQuantity,
		"quantity":           req.Quantity,
		"price":              req.Price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &kassa_service.SaleProduct{
		Id:                req.Id,
		BrandId:           req.BrandId,
		CategoryId:        req.CategoryId,
		ProductName:       req.ProductName,
		Barcode:           req.Barcode,
		RemainingQuantity: req.RemainingQuantity,
		Quantity:          req.Quantity,
		Price:             req.Price,
		TotalPrice:        req.TotalPrice,
	}, nil
}

func (r *SaleProductRepo) Delete(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sale_products WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
