package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/pkg/helper"
)

type SmenaRepo struct {
	db *pgxpool.Pool
}

func NewSmenaRepo(db *pgxpool.Pool) *SmenaRepo {
	return &SmenaRepo{
		db: db,
	}
}

func (r *SmenaRepo) Create(ctx context.Context, req *kassa_service.SmenaCreate) (*kassa_service.Smena, error) {
	count, err := r.GetList(ctx, &kassa_service.SmenaGetListRequest{})
	var (
		id      = uuid.New().String()
		smenaId = helper.MakeId("С", int(count.Count)+1)
		query   string
	)

	query = `
		INSERT INTO smena(id, smena_id, branch_id, employee_id, sales_address_id, status)
		VALUES ($1, $2, $3, $4, $5, $6)
	`

	_, err = r.db.Exec(ctx, query,
		id,
		smenaId,
		req.BranchId,
		req.EmployeeId,
		req.SalesAddressId,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Smena{
		Id:             id,
		SmenaId:        smenaId,
		BranchId:       req.BranchId,
		EmployeeId:     req.EmployeeId,
		SalesAddressId: req.SalesAddressId,
		Status:         req.Status,
	}, nil
}

func (r *SmenaRepo) GetByID(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (*kassa_service.Smena, error) {
	var (
		query string

		id             sql.NullString
		smenaId        string
		branchId       sql.NullString
		employeeId     sql.NullString
		salesAddressId sql.NullString
		status         string
	)

	query = `
		SELECT
			id, smena_id, branch_id, employee_id, sales_address_id, status
		FROM smena
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&smenaId,
		&branchId,
		&employeeId,
		&salesAddressId,
		&status,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Smena{
		Id:             id.String,
		SmenaId:        smenaId,
		BranchId:       branchId.String,
		EmployeeId:     employeeId.String,
		SalesAddressId: salesAddressId.String,
		Status:         status,
	}, nil
}

func (r *SmenaRepo) GetList(ctx context.Context, req *kassa_service.SmenaGetListRequest) (*kassa_service.SmenaGetListResponse, error) {

	var (
		resp   = &kassa_service.SmenaGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, branch_id, employee_id, sales_address_id, status	
		FROM smena
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByEmployee != "" {
		where += ` AND employee_id ILIKE '%' || '` + req.SearchByEmployee + `' || '%'`
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			smenaId        string
			branchId       sql.NullString
			employeeId     sql.NullString
			salesAddressId sql.NullString
			status         string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&smenaId,
			&branchId,
			&employeeId,
			&salesAddressId,
			&status,
		)

		if err != nil {
			return nil, err
		}

		resp.Smena = append(resp.Smena, &kassa_service.Smena{
			Id:             id.String,
			SmenaId:        smenaId,
			BranchId:       branchId.String,
			EmployeeId:     employeeId.String,
			SalesAddressId: salesAddressId.String,
			Status:         status,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SmenaRepo) Update(ctx context.Context, req *kassa_service.SmenaUpdate) (*kassa_service.Smena, error) {

	var (
		query  string
		params map[string]interface{}
	)
	query = `
		UPDATE
			smena
		SET
		    branch_id = :branch_id,
		    employee_id = :employee_id,
		    sales_address_id = :sales_address_id,
		    status = :status,
		    smena_id = :smena_id
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":               req.Id,
		"branch_id":        req.BranchId,
		"employee_id":      req.EmployeeId,
		"sales_address_id": req.SalesAddressId,
		"status":           req.Status,
		"smena_id":         req.SmenaId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &kassa_service.Smena{
		Id:             req.Id,
		SmenaId:        req.SmenaId,
		BranchId:       req.BranchId,
		EmployeeId:     req.EmployeeId,
		SalesAddressId: req.SalesAddressId,
		Status:         req.Status,
	}, nil
}

func (r *SmenaRepo) Delete(ctx context.Context, req *kassa_service.SmenaPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM smena WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
