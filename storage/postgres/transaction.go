package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/kassa_service/pkg/helper"
)

type TransactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) *TransactionRepo {
	return &TransactionRepo{
		db: db,
	}
}

func (r *TransactionRepo) Create(ctx context.Context, req *kassa_service.TransactionCreate) (*kassa_service.Transaction, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO transaction(id, cash, uzcard, payme, click, humo, apelsin, visa, smena_id, total_price)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		req.Visa,
		req.SmenaId,
		req.TotalPrice,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Transaction{
		Id:         id,
		Cash:       req.Cash,
		Uzcard:     req.Uzcard,
		Payme:      req.Payme,
		Click:      req.Click,
		Humo:       req.Humo,
		Apelsin:    req.Apelsin,
		Visa:       req.Visa,
		SmenaId:    req.SmenaId,
		TotalPrice: req.TotalPrice,
	}, nil
}

func (r *TransactionRepo) GetByID(ctx context.Context, req *kassa_service.TransactionPrimaryKey) (*kassa_service.Transaction, error) {
	var (
		query      string
		id         sql.NullString
		cash       int64
		uzcard     int64
		payme      int64
		click      int64
		humo       int64
		apelsin    int64
		visa       int64
		smenaId    string
		totalPrice int64
	)

	query = `
		SELECT
			id, cash, uzcard, payme, click, humo, apelsin, visa, smena_id, total_price
		FROM transaction
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&visa,
		&smenaId,
		&totalPrice,
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.Transaction{
		Id:         id.String,
		Cash:       cash,
		Uzcard:     uzcard,
		Payme:      payme,
		Click:      click,
		Humo:       humo,
		Apelsin:    apelsin,
		Visa:       visa,
		SmenaId:    smenaId,
		TotalPrice: totalPrice,
	}, nil
}

func (r *TransactionRepo) GetList(ctx context.Context, req *kassa_service.TransactionGetListRequest) (*kassa_service.TransactionGetListResponse, error) {

	var (
		resp   = &kassa_service.TransactionGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, cash, uzcard, payme, click, humo, apelsin, visa, smena_id, total_price	
		FROM transaction
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchBySmenaId != "" {
		where += ` AND smena_id ILIKE '%' || '` + req.SearchBySmenaId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			cash       int64
			uzcard     int64
			payme      int64
			click      int64
			humo       int64
			apelsin    int64
			visa       int64
			smenaId    string
			totalPrice int64
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&visa,
			&smenaId,
			&totalPrice,
		)

		if err != nil {
			return nil, err
		}

		resp.Transactions = append(resp.Transactions, &kassa_service.Transaction{
			Id:         id.String,
			Cash:       cash,
			Uzcard:     uzcard,
			Payme:      payme,
			Click:      click,
			Humo:       humo,
			Apelsin:    apelsin,
			Visa:       visa,
			SmenaId:    smenaId,
			TotalPrice: totalPrice,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *TransactionRepo) Update(ctx context.Context, req *kassa_service.TransactionUpdate) (*kassa_service.Transaction, error) {

	var (
		query  string
		params map[string]interface{}
	)
	query = `
		UPDATE
			transaction
		SET
		    cash = :cash,
		    uzcard = :uzcard,
		    payme = :payme,
		    click = :click,
		    humo = :humo,
		    apelsin = :apelsin,
		    visa = :visa,
		    smena_id = :smena_id,
		    total_price = :total_price
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.Id,
		"cash":        req.Cash,
		"uzcard":      req.Uzcard,
		"payme":       req.Payme,
		"click":       req.Click,
		"humo":        req.Humo,
		"apelsin":     req.Apelsin,
		"visa":        req.Visa,
		"smena_id":    req.SmenaId,
		"total_price": req.TotalPrice,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &kassa_service.Transaction{
		Id:         req.Id,
		Cash:       req.Cash,
		Uzcard:     req.Uzcard,
		Payme:      req.Payme,
		Click:      req.Click,
		Humo:       req.Humo,
		Apelsin:    req.Apelsin,
		Visa:       req.Visa,
		SmenaId:    req.SmenaId,
		TotalPrice: req.TotalPrice,
	}, nil
}

func (r *TransactionRepo) Delete(ctx context.Context, req *kassa_service.TransactionPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM transaction WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
