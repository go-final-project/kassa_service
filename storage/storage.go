package storage

import (
	"context"
	"gitlab.com/GoFinalProject/kassa_service/genproto/kassa_service"
)

type StorageI interface {
	CloseDB()
	Sale() SaleRepoI
	Smena() SmenaRepoI
	SaleProduct() SaleProductRepoI
	Payment() PaymentRepoI
	Transaction() TransactionRepoI
}

type SaleRepoI interface {
	Create(context.Context, *kassa_service.SaleCreate) (*kassa_service.Sale, error)
	GetByID(context.Context, *kassa_service.SalePrimaryKey) (*kassa_service.Sale, error)
	GetList(context.Context, *kassa_service.SaleGetListRequest) (*kassa_service.SaleGetListResponse, error)
	Update(context.Context, *kassa_service.SaleUpdate) (*kassa_service.Sale, error)
	Delete(context.Context, *kassa_service.SalePrimaryKey) error
}

type SmenaRepoI interface {
	Create(context.Context, *kassa_service.SmenaCreate) (*kassa_service.Smena, error)
	GetByID(context.Context, *kassa_service.SmenaPrimaryKey) (*kassa_service.Smena, error)
	GetList(context.Context, *kassa_service.SmenaGetListRequest) (*kassa_service.SmenaGetListResponse, error)
	Update(context.Context, *kassa_service.SmenaUpdate) (*kassa_service.Smena, error)
	Delete(context.Context, *kassa_service.SmenaPrimaryKey) error
}

type SaleProductRepoI interface {
	Create(context.Context, *kassa_service.SaleProductCreate) (*kassa_service.SaleProduct, error)
	GetByID(context.Context, *kassa_service.SaleProductPrimaryKey) (*kassa_service.SaleProduct, error)
	GetList(context.Context, *kassa_service.SaleProductGetListRequest) (*kassa_service.SaleProductGetListResponse, error)
	Update(context.Context, *kassa_service.SaleProductUpdate) (*kassa_service.SaleProduct, error)
	Delete(context.Context, *kassa_service.SaleProductPrimaryKey) error
}

type PaymentRepoI interface {
	Create(context.Context, *kassa_service.PaymentCreate) (*kassa_service.Payment, error)
	GetByID(context.Context, *kassa_service.PaymentPrimaryKey) (*kassa_service.Payment, error)
	GetList(context.Context, *kassa_service.PaymentGetListRequest) (*kassa_service.PaymentGetListResponse, error)
	Update(context.Context, *kassa_service.PaymentUpdate) (*kassa_service.Payment, error)
	Delete(context.Context, *kassa_service.PaymentPrimaryKey) error
}

type TransactionRepoI interface {
	Create(context.Context, *kassa_service.TransactionCreate) (*kassa_service.Transaction, error)
	GetByID(context.Context, *kassa_service.TransactionPrimaryKey) (*kassa_service.Transaction, error)
	GetList(context.Context, *kassa_service.TransactionGetListRequest) (*kassa_service.TransactionGetListResponse, error)
	Update(context.Context, *kassa_service.TransactionUpdate) (*kassa_service.Transaction, error)
	Delete(context.Context, *kassa_service.TransactionPrimaryKey) error
}
